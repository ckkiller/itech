<?php declare(strict_types=1);

namespace App;

use App\Exception\ApiException;
use App\Model\Response;
use App\Service\DependencyManager;
use App\Service\Router;

class Kernel
{
    public function __construct()
    {
        $dependencyManager = new DependencyManager();
        try {
            /** @var Router $router */
            $router = $dependencyManager->get(Router::class);

            /** @var Response $response */
            $response = $router->resolve($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);

            http_response_code($response->getResponseCode());
            foreach ($response->getHeaders() as $header => $value) {
                header("{$header}: {$value}");
            }

            echo json_encode($response->getContent());
        } catch (ApiException $e) {
            http_response_code($e->getCode());
            header('Content-Type: application/json');

            echo json_encode([
                'status' => $e->getCode(),
                'error' => $e->getMessage(),
            ]);
        }
    }
}