<?php declare(strict_types=1);

namespace App\Controller;

use App\Exception\InternalServerErrorException;
use App\Model\Response;
use App\Service\TwitterApiHandler;
use App\Exception\ApiException;

class Twitter
{
    /**
     * @var TwitterApiHandler
     */
    private $apiHandler;

    /**
     * Twitter constructor.
     * @param TwitterApiHandler $apiHandler
     */
    public function __construct(TwitterApiHandler $apiHandler)
    {
        $this->apiHandler = $apiHandler;
    }

    /**
     * @Get(/twitter/{username})
     *
     * @param string $username
     * @return Response
     * @throws ApiException
     */
    public function getTweets(string $username): Response
    {
        $response = $this->apiHandler->getTweets($username);
        return new Response($response);
    }

    /**
     * @Get(/twitter/{username}/{id})
     *
     * @param string $username
     * @param string $id
     * @return Response
     * @throws ApiException
     */
    public function getTweetsFrom(string $username, string $id): Response
    {
        $response = $this->apiHandler->getTweets($username, $id);
        return new Response($response);
    }
}