<?php
namespace App\Service;

use App\Exception\InternalServerErrorException;
use App\Exception\InvalidConfigurationException;

final class DependencyManager
{
    /**
     * @var array
     */
    private $cache = [];

    /**
     * DependencyManager constructor.
     */
    public function __construct()
    {
        $this->cache[self::class] = $this;
    }

    /**
     * @param string $className
     * @return mixed
     * @throws InternalServerErrorException
     * @throws InvalidConfigurationException
     */
    public function get(string $className)
    {
        if (!array_key_exists($className, $this->cache)) {
            try {
                $reflect = new \ReflectionClass($className);
            } catch (\ReflectionException $e) {
                throw new InternalServerErrorException($e);
            }

            $constructor = $reflect->getConstructor();
            $parameters = [];
            if (!is_null($constructor)) {
                $parameters = $this->getParameters($constructor->getParameters());
            }

            $instance = $reflect->newInstanceArgs($parameters);
            $this->cache[$className] = $instance;
        }

        return $this->cache[$className];
    }

    /**
     * @param array $parameters
     * @return array
     * @throws InternalServerErrorException
     * @throws InvalidConfigurationException
     */
    private function getParameters(array $parameters)
    {
        $initParams = [];
        foreach ($parameters as $param) {
            $type = $param->getType();
            if ($type === null) {
                continue;
            }
            $typeName = $type->getName();

            // All classes will start with an upper case character, all other variable types will start with a lower
            // case character
            if (preg_match('/\p{Lu}/u', $typeName)) {
                $initParams[] = $this->get($typeName);
            } else if ($param->isOptional()) {
                continue;
            } else {
                throw new InvalidConfigurationException();
            }
        }

        return $initParams;
    }
}