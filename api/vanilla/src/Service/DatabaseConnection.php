<?php declare(strict_types=1);

namespace App\Service;

class DatabaseConnection
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * DatabaseConnection constructor.
     * @param Environment $env
     */
    public function __construct(Environment $env)
    {
        $this->pdo = new \PDO($env->get('MYSQL_DSN'), $env->get('MYSQL_USERNAME'), $env->get('MYSQL_PASSWORD'));
    }

    /**
     * @param string $query
     * @param array|null $params
     * @param bool $single
     * @return array|bool
     */
    public function get(string $query, ?array $params = null, $single = false)
    {
        $statement = $this->run($query, $params);

        if (!$statement->rowCount()) {
            return false;
        }

        if ($single) {
            return $statement->fetch();
        } else {
            return $statement->fetchAll();
        }
    }

    /**
     * @param string $query
     * @param array|null $params
     * @return bool|int
     */
    public function insert(string $query, ?array $params = null)
    {
        $statement = $this->run($query, $params);

        if (in_array($statement->errorCode(), ['', '00000'])) {
            return (int) $this->pdo->lastInsertId();
        }

        return false;
    }

    /**
     * @param string $query
     * @param array|null $params
     * @return bool
     */
    public function update(string $query, ?array $params = null)
    {
        $statement = $this->run($query, $params);

        return in_array($statement->errorCode(), ['', '00000']);
    }

    /**
     * @param string $query
     * @param array|null $params
     * @return \PDOStatement
     */
    private function run(string $query, ?array $params = null): \PDOStatement
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($params ?? []);
        return $statement;
    }
}