<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\CachePermissionException;

class CacheHandler
{
    /**
     * @var string
     */
    private $cacheDir;

    /**
     * CacheHandler constructor.
     * @param Environment $env
     * @param string|null $root
     */
    public function __construct(Environment $env, string $root = null)
    {
        if (is_null($root)) {
            $root = $env->get('PROJECT_ROOT') . '/cache/';
        }

        $this->cacheDir = $root;
    }

    /**
     * @param string $file
     * @return false|\stdClass
     */
    public function read(string $file): ?\stdClass
    {
        $file .= '.json';
        if (!file_exists($this->cacheDir . $file)) {
            return null;
        }

        $contents = json_decode(file_get_contents($this->cacheDir . $file));

        if ($contents) {
            return $contents;
        } else {
            return null;
        }
    }

    /**
     * @param string $file
     * @param $content
     * @return bool
     * @throws CachePermissionException
     */
    public function write(string $file, $content): bool
    {
        $file .= '.json';
        $content = json_encode($content);
        if (!$content) {
            return false;
        }

        if (!file_put_contents($this->cacheDir . $file, $content)) {
            throw new CachePermissionException();
        }

        return true;
    }
}