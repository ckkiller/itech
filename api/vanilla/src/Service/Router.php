<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\CachePermissionException;
use App\Exception\InvalidConfigurationException;
use App\Exception\ApiException;
use App\Exception\UnknownRouteException;
use App\Model\Response;

class Router
{
    /**
     * @var DependencyManager
     */
    private $dependencyManager;

    /**
     * @var CacheHandler
     */
    private $cacheHandler;

    /**
     * @var Environment
     */
    private $env;

    /**
     * @var \stdClass
     */
    private $routes;

    /**
     * Router constructor.
     * @param DependencyManager $dependencyManager
     * @param CacheHandler $cacheHandler
     * @param Environment $env
     * @throws ApiException
     */
    public function __construct(DependencyManager $dependencyManager, CacheHandler $cacheHandler, Environment $env)
    {
        $this->dependencyManager = $dependencyManager;
        $this->cacheHandler = $cacheHandler;
        $this->env = $env;

        $root = $this->env->get('PROJECT_ROOT');
        try {
            $this->routes = $this->getRoutes($root);
        } catch (InvalidConfigurationException $e) {
            throw new InvalidConfigurationException($e);
        }
    }

    /**
     * @param string $root
     * @return \stdClass
     * @throws InvalidConfigurationException
     */
    public function getRoutes(string $root): \stdClass
    {
        if ($routes = $this->cacheHandler->read('routes')) {
            return $routes;
        } else {
            try {
                return $this->cacheRoutes($root);
            } catch (InvalidConfigurationException $e) {
                throw new InvalidConfigurationException($e);
            }
        }
    }

    /**
     * @param string $root
     * @return \stdClass
     * @throws InvalidConfigurationException
     */
    public function cacheRoutes(string $root): \stdClass
    {
        $files = scandir($root . '/src/Controller');
        if (!$files) {
            throw new InvalidConfigurationException();
        }

        $routes = [
            'Get' => [],
            'Post' => [],
            'Put' => [],
            'Patch' => [],
            'Delete' => [],
            'Options' => [],
        ];

        foreach ($files as $file) {
            if (!preg_match('/\.php$/', $file)) {
                continue;
            }

            try {
                $className = 'App\\Controller\\' . substr($file, 0, -4);
                $reflect = new \ReflectionClass($className);
                $methods = $reflect->getMethods();
            } catch (\ReflectionException $e) {
                throw new InvalidConfigurationException($e);
            }

            foreach ($methods as $method) {
                $annotation = $method->getDocComment();
                if (!$annotation || !$method->isPublic()) {
                    continue;
                }

                preg_match('/\@(Get|Post|Put|Patch|Delete|Options)\(([^)]+)\)/', $annotation, $matches);
                if (empty($matches)) {
                    continue;
                }

                $routeParams = [];
                $url = $matches[2];
                $parameters = $method->getParameters();

                foreach ($parameters as $param) {
                    $name = $param->getName();
                    $url = str_replace("{{$name}}", '([^/]+)', $url);

                    $routeParams[] = $param->getType()->getName();
                }

                $routes[$matches[1]]['/^' . str_replace('/', '\\/', $url) . '$/'] = [
                    'class' => '\\' . $className,
                    'method' => $method->getName(),
                    'params' => $routeParams,
                ];
            }
        }

        try {
            $this->cacheHandler->write('routes', $routes);
        } catch (CachePermissionException $e) {
            throw new InvalidConfigurationException($e);
        }

        // Simulate how we'd get this from the cache
        return json_decode(json_encode($routes));
    }

    /**
     * @param string $url
     * @param string $method
     * @return Response
     * @throws ApiException
     */
    public function resolve(string $url, string $method): Response
    {
        foreach ($this->routes->{ucfirst(strtolower($method))} as $route => $data) {
            if (preg_match($route, $url, $matches)) {
                $args = [];

                if ($matches) {
                    array_shift($matches);
                    foreach ($matches as $index => $match) {
                        settype($match, $data->params[$index]);
                        $args[] = $match;
                    }
                }

                $instance = $this->dependencyManager->get($data->class);
                return call_user_func_array([$instance, $data->method], $args);
            }
        }

        throw new UnknownRouteException();
    }
}