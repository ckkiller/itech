<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\DatabaseException;
use App\Exception\ExternalApiException;
use App\Repository\TwitterRepository;

class TwitterApiHandler extends ExternalRestApiHandler
{
    const DOMAIN = 'https://api.twitter.com';
    const VERSION = '/1.1';

    /**
     * @var Environment
     */
    private $env;

    /**
     * @var TwitterRepository
     */
    private $repository;

    /**
     * TwitterApiHandler constructor.
     * @param Environment $env
     * @param TwitterRepository $twitterRepository
     */
    public function __construct(Environment $env, TwitterRepository $twitterRepository)
    {
        $this->env = $env;
        $this->repository = $twitterRepository;
    }

    /**
     * @return string
     * @throws ExternalApiException
     */
    public function authenticate(): string
    {
        $key = $this->env->get('TWITTER_API_KEY') . ':' . $this->env->get('TWITTER_API_SECRET_KEY');
        $key = base64_encode($key);

        try {
            $response = $this->request(
                self::DOMAIN . '/oauth2/token?grant_type=client_credentials',
                self::HTTP_POST,
                null,
                ['Authorization: Basic ' . $key]
            );
        } catch(ExternalApiException $e) {
            throw new ExternalApiException($e);
        }

        if (!isset($response->access_token)) {
            throw new ExternalApiException();
        }

        return $response->access_token;
    }

    /**
     * @param string $username
     * @param string|null $id
     * @return array
     * @throws DatabaseException
     * @throws ExternalApiException
     */
    public function getTweets(string $username, ?string $id = null): array
    {
        $token = $this->getToken();
        try {
            $queryString = ['screen_name' => $username];
            if (!is_null($id)) {
                $queryString['since_id'] = $id;
            }

            $response = $this->request(
                self::DOMAIN . self::VERSION . "/statuses/user_timeline.json",
                self::HTTP_GET,
                null,
                ['Authorization: Bearer ' . $token],
                $queryString
            );
        } catch (ExternalApiException $e) {
            throw new ExternalApiException($e);
        }

        return $response;
    }

    /**
     * @return string
     * @throws DatabaseException
     * @throws ExternalApiException
     */
    private function getToken(): string
    {
        $token = $this->repository->getToken();

        if (!$token) {
            try {
                $token = $this->authenticate();
                $status = $this->repository->storeToken($token);
            } catch (ExternalApiException $e) {
                throw new ExternalApiException($e);
            }

            if (!$status) {
                throw new DatabaseException();
            }

            return $token;
        } else {
            return $token['token'];
        }
    }
}