<?php
namespace App\Service;

use App\Exception\InvalidConfigurationException;

class Environment
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Environment constructor.
     * @param string|null $env
     * @throws InvalidConfigurationException
     */
    public function __construct(string $env = null)
    {
        if (is_null($env)) {
            $env = __DIR__ . '/../../.env';
        }

        $file = file_get_contents($env);
        if (!$file) {
            throw new InvalidConfigurationException();
        }

        $data = explode("\n", $file);
        foreach ($data as $item) {
            $pair = explode('=', $item, 2);
            $this->data[$pair[0]] = $pair[1];
        }
    }

    /**
     * @param string $key
     * @return bool|string
     */
    public function get(string $key)
    {
        return $this->data[$key] ?? false;
    }
}