<?php declare(strict_types=1);

namespace App\Model;

class Response
{
    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var array|null
     */
    private $content = [];

    /**
     * @var int
     */
    private $responseCode = 204;

    /**
     * Response constructor.
     * @param array|null $content
     * @param int|null $code
     * @param array $headers
     */
    public function __construct(?array $content = null, ?int $code = null, array $headers = [])
    {
        $this->headers = array_merge([
            'Content-Type' => 'application/json'
        ], $headers);

        $this->content = $content;

        if (!is_null($code)) {
            $this->responseCode = $code;
        } else if (count($content)) {
            $this->responseCode = 200;
        }
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array|null
     */
    public function getContent(): ?array
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }
}