<?php declare(strict_types = 1);

namespace App\Exception;

use Throwable;

class InvalidConfigurationException extends \Exception implements ApiException
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_INVALID_CONFIGURATION,
            self::ERROR_INVALID_CONFIGURATION_CODE,
            $previous
        );
    }
}