<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class InternalServerErrorException extends \Exception implements ApiException
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_INTERNAL_SERVER_ERROR,
            self::ERROR_INTERNAL_SERVER_ERROR_CODE,
            $previous
        );
    }
}