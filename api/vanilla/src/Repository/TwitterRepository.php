<?php declare(strict_types=1);

namespace App\Repository;

use App\Service\DatabaseConnection;

class TwitterRepository extends AbstractRepository
{
    /**
     * @return array|bool
     */
    public function getToken()
    {
        return $this->db->get(
            'SELECT token 
            FROM vanilla_twitter_auth 
            ORDER BY id DESC
            LIMIT 1',
            null,
            true
        );
    }

    /**
     * @param string $token
     * @return bool|int
     */
    public function storeToken(string $token)
    {
        return $this->db->insert(
            'INSERT INTO vanilla_twitter_auth (token) VALUES (:token)',
            [':token' => $token]
        );
    }
}