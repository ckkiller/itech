<?php declare(strict_types=1);

namespace App\Repository;

use App\Service\DatabaseConnection;

abstract class AbstractRepository
{
    /**
     * @var DatabaseConnection
     */
    protected $db;

    /**
     * TwitterRepository constructor.
     * @param DatabaseConnection $db
     */
    public function __construct(DatabaseConnection $db)
    {
        $this->db = $db;
    }

}