<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="framework_twitter_auth")
 */
class TwitterToken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     * @var string
     */
    private $token;

    /**
     * @ORM\Column(name="requested_at", type="datetime")
     * @var \DateTime
     */
    private $requestedAt;

    /**
     * TwitterToken constructor.
     * @param string $token
     * @throws \Exception
     */
    public function __construct(string $token)
    {
        $this->requestedAt = new \DateTime();
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TwitterToken
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return TwitterToken
     */
    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRequestedAt(): \DateTime
    {
        return $this->requestedAt;
    }

    /**
     * @param \DateTime $requestedAt
     * @return TwitterToken
     */
    public function setRequestedAt(\DateTime $requestedAt): self
    {
        $this->requestedAt = $requestedAt;
        return $this;
    }
}