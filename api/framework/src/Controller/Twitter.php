<?php declare(strict_types=1);

namespace App\Controller;

use App\Exception\ApiException;
use App\Service\TwitterApiHandler;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class Twitter extends AbstractController
{
    /**
     * @var TwitterApiHandler
     */
    private $apiHandler;

    /**
     * Twitter constructor.
     * @param TwitterApiHandler $apiHandler
     */
    public function __construct(TwitterApiHandler $apiHandler)
    {
        $this->apiHandler = $apiHandler;
    }

    /**
     * @Route("/twitter/{username}", methods={"GET"})
     *
     * @param string $username
     * @return Response
     * @throws ApiException
     */
    public function getTweets(string $username): Response
    {
        $tweets = $this->apiHandler->getTweets($username);
        return $this->createResponse($tweets);
    }

    /**
     * @Route("/twitter/{username}/{id}", methods={"GET"})
     *
     * @param string $username
     * @param string $id
     * @return Response
     * @throws ApiException
     */
    public function getTweetsFrom(string $username, string $id): Response
    {
        $tweets = $this->apiHandler->getTweets($username, $id);
        return $this->createResponse($tweets);
    }
}