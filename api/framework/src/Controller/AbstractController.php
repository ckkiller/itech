<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

abstract class AbstractController
{
    protected function createResponse(array $content, ?int $status = null, array $headers = [])
    {
        if (is_null($status)) {
            $status = empty($content) ? 204 : 200;
        }

        $headers = array_merge([
            'Content-Type' => 'application/json;charset=UTF8',
        ], $headers);

        return new Response(
            json_encode($content),
            $status,
            $headers
        );
    }
}