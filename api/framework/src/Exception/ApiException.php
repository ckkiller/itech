<?php declare(strict_types = 1);

namespace App\Exception;

interface ApiException
{
    const ERROR_INTERNAL_SERVER_ERROR = 'Internal server error';
    const ERROR_INTERNAL_SERVER_ERROR_CODE = 500;

    const ERROR_INVALID_CONFIGURATION = 'Invalid backend configuration';
    const ERROR_INVALID_CONFIGURATION_CODE = 500;

    const ERROR_CACHE_PERMISSION = 'Unable to write to internal cache';
    const ERROR_CACHE_PERMISSION_CODE = 500;

    const ERROR_UNKNOWN_ROUTE = 'Resource not found';
    const ERROR_UNKNOWN_ROUTE_CODE = 404;

    const ERROR_External_API_FAILURE = 'An external API call failed';
    const ERROR_External_API_FAILURE_CODE = 500;

    const ERROR_DATABASE_ERROR = 'Database error';
    const ERROR_DATABASE_ERROR_CODE = 500;
}