<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class ExternalApiException extends \Exception implements ApiException
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_External_API_FAILURE,
            self::ERROR_External_API_FAILURE_CODE,
            $previous
        );
    }
}