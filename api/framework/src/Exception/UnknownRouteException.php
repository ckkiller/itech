<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class UnknownRouteException extends \Exception implements ApiException
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_UNKNOWN_ROUTE,
            self::ERROR_UNKNOWN_ROUTE_CODE,
            $previous
        );
    }
}