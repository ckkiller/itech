<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class CachePermissionException extends \Exception implements ApiException
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_CACHE_PERMISSION,
            self::ERROR_CACHE_PERMISSION_CODE,
            $previous
        );
    }
}