<?php declare(strict_types=1);

namespace App\Exception;

use Throwable;

class DatabaseException extends \Exception implements ApiException
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            self::ERROR_DATABASE_ERROR,
            self::ERROR_DATABASE_ERROR_CODE,
            $previous
        );
    }
}