<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\TwitterToken;
use App\Exception\DatabaseException;
use App\Exception\ExternalApiException;
use App\Exception\InternalServerErrorException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Dotenv\Dotenv;

class TwitterApiHandler extends ExternalRestApiHandler
{
    const DOMAIN = 'https://api.twitter.com';
    const VERSION = '/1.1';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TwitterApiHandler constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return string
     * @throws ExternalApiException
     */
    public function authenticate(): string
    {
        $key = $_ENV['TWITTER_API_KEY'] . ':' . $_ENV['TWITTER_API_SECRET_KEY'];
        $key = base64_encode($key);

        try {
            $response = $this->request(
                self::DOMAIN . '/oauth2/token?grant_type=client_credentials',
                self::HTTP_POST,
                null,
                ['Authorization: Basic ' . $key]
            );
        } catch(ExternalApiException $e) {
            throw new ExternalApiException($e);
        }

        if (!isset($response->access_token)) {
            throw new ExternalApiException();
        }

        return $response->access_token;
    }

    /**
     * @param string $username
     * @param string|null $id
     * @return array
     * @throws DatabaseException
     * @throws ExternalApiException
     * @throws InternalServerErrorException
     */
    public function getTweets(string $username, ?string $id = null): array
    {
        $token = $this->getToken();
        try {
            $queryString = ['screen_name' => $username];
            if (!is_null($id)) {
                $queryString['since_id'] = $id;
            }

            $response = $this->request(
                self::DOMAIN . self::VERSION . "/statuses/user_timeline.json",
                self::HTTP_GET,
                null,
                ['Authorization: Bearer ' . $token],
                $queryString
            );
        } catch (ExternalApiException $e) {
            throw new ExternalApiException($e);
        }

        return $response;
    }

    /**
     * @return string
     * @throws ExternalApiException
     * @throws InternalServerErrorException
     */
    private function getToken(): string
    {
        $repository = $this->em->getRepository(TwitterToken::class);

        /** @var TwitterToken $token */
        $token = $repository->findBy([], ['id' => 'DESC'], 0, 1);

        if (!$token) {
            try {
                $token = $this->authenticate();
                $token = new TwitterToken($token);
                $this->em->persist($token);
                $this->em->flush();
            } catch (ExternalApiException $e) {
                throw new ExternalApiException($e);
            } catch (\Exception $e) {
                throw new InternalServerErrorException($e);
            }

            return $token->getToken();
        } else {
            return $token->getToken();
        }
    }
}