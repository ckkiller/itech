<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\ExternalApiException;

class ExternalRestApiHandler
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_PUT = 'PUT';
    const HTTP_PATCH = 'PATCH';
    const HTTP_DELETE = 'DELETE';
    const HTTP_OPTIONS = 'OPTIONS';

    /**
     * @param string $url
     * @param string $type
     * @param array|null $content
     * @param array $headers
     * @param array $queryString
     * @return mixed
     * @throws ExternalApiException
     */
    public function request(string $url, string $type, ?array $content = null, array $headers = [], array $queryString = [])
    {
        $payload = '';
        $qs = '';

        if (!empty($queryString)) {
            $qs = [];
            foreach ($queryString as $key => $value) {
                $qs[] = "{$key}=" . urlencode($value);
            }
            $qs = '?' . implode('&', $qs);
        }

        $curl = curl_init($url . $qs);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);

        if (!empty($content)) {
            $payload = json_encode($content);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        }

        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array_merge(
                $headers,
                [
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen($payload),
                ]
            )
        );

        $response = curl_exec($curl);
        if ($response) {
            return json_decode($response);
        } else {
            throw new ExternalApiException();
        }
    }
}