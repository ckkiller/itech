### Prerequisites
- Docker / docker-compose
- *nix shell terminal

### Installation
1. Make sure no applications are using ports 80, 443 & 3306
2. Add virtual host names to your hosts file (itech.vanilla, api.itech.vanilla, 
itech.framework, api.itech.framework) under the IP 127.0.0.1... Or run 
`sudo ./scripts/hosts.sh` if you're using Linux or OSX (not tested with Windows)
3. Start the docker environment `docker-compose up`

### Usage
**Please Note**: Because the ssl certificates are self-signed, some browsers will show 
a security alert before allowing access to the site. This can also be the case for pages 
called via AJAX, if the browser you're using does this, you will need to open up 
`https://api.itech.vanilla/twitter/ubuntu` and `https://api.itech.framework/twitter/ubuntu`
in separate tabs and 'proceed' to the site (see 'README Resources/' for screenshot examples).

Open `https://ìtech.vanilla` or `https://itech.framework` within a browser - these domains
are for the UI widget. 

The interface should consist of: 
- A simple input for the twitter screen name (defaults to'ubuntu'). 
- A button to switch the user and fetch the tweets for that user (hitting ENTER on the 
username input will do the same as clicking this button).
- A display for the tweets themselves (scrollable). Each tweet within this can contain
    - Original text content
    - Date and time it was posted
    - links (clickable)
    - hashtags (clickable)
    - user references (@user) (clickable)
- An information display (when tweets are retrieved). 
- A loading indicator to the right of the input, which displays/spins when the api is 
being called.



## Dev Notes
These steps have already been carried out, this is purely informative. 

- Vanilla UI Compiling 
```
cd /path/to/repo/ui/vanilla
npm install
./node_modules/gulp/bin/gulp.js
```

- Framework UI Compiling
```
cd /path/to/repo/ui/framework
npm install
npm run build
```