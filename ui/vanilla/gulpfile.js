const { series, parallel, src, dest } = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

function clean() {

}

function concatJavaScript() {
    return src(['./scripts/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(dest('./public/'));
}

function concatScss() {
    return src(['./styles/*.scss'])
        .pipe(concat('styles.scss'))
        .pipe(dest('./dist/'));
}

function compileScss() {
    return src(['./dist/styles.scss'])
        .pipe(sass({
            includePaths: ['scss']
        }))
        .pipe(dest('./public/'));
}

exports.default = series(
    // clean,
    parallel(
        concatJavaScript,
        series(concatScss, compileScss)
    )
);