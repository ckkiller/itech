class Api
{
    /**
     * @param {function} loading
     */
    constructor(loading)
    {
        this.loading = loading;
    }

    /**
     * @param {string} username
     * @param {string} id
     * @returns {Promise<string>}
     */
    get(username, id)
    {
        this.loading(true);
        let url = `https://api.itech.vanilla/twitter/${username}`;
        if (typeof id !== 'undefined') {
            url += `/${id}`;

        }
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.addEventListener('readystatechange', () => {
                if (xhr.readyState === 4) {
                    if ([200, 204].includes(xhr.status)) {
                        resolve(xhr.responseText);
                    } else {
                        reject(xhr.responseText);
                    }

                    this.loading(false);
                }
            }, false);

            xhr.open('GET', url, true);
            xhr.send();
        });
    }
}
window.onload = () => {
    const tweetDisplay = document.querySelector('.tweets-container');
    const usernameInput = document.querySelector('.user-strip > input');
    const button = document.querySelector('.switch-user-button');
    const loadingIndicator = document.querySelector('.loading');
    const notificationsContainer = document.querySelector('.notifications-container');

    const api = new Api(active => loadingIndicator.classList.toggle('visible', active));
    const notification = new Notification(notificationsContainer);
    new Twitter(api, notification,{ usernameInput, button }, tweetDisplay);
};
class Notification
{
    constructor(container)
    {
        this.container = container;
    }

    show(message, type)
    {
        let notice = document.createElement('section');
        notice.innerHTML = message;
        notice.classList.add('notice', type);

        this.container.appendChild(notice);
        setTimeout(() => this.container.removeChild(notice), 8000);
    }
}
class Twitter
{
    /**
     * @param {Api} api
     * @param {Notification} notification
     * @param {object} controls
     * @param {Node} display
     */
    constructor(api, notification, controls, display)
    {
        this.api = api;
        this.notification = notification;
        this.displayContainer = display;
        this.updateInterval = 15000;

        let username = this.getUsername();
        if (!username) {
            username = 'ubuntu';
            this.setUsername(username);
        }
        controls.usernameInput.value = username;

        this.bindListeners(controls);
    }

    /**
     * @param {Node} usernameInput
     * @param {Node} button
     */
    bindListeners({ usernameInput, button })
    {
        this.usernameInput = usernameInput;

        button.addEventListener('click', () => {
            const username = this.usernameInput.value;
            this.setUsername(username);
            clearTimeout(this.interval);

            this.api.get(username)
                .then(response => this.refreshDisplay(response))
                .catch(response => {
                    const data = JSON.parse(response);
                    this.notification.show(data.error, 'failure');
                    this.interval = setTimeout(() => this.getUpdates(), this.updateInterval);
                });
        }, false);

        usernameInput.addEventListener('keyup', e => {
            if (e.keyCode === 13) {
                button.click();
            }
        }, false);

        // load initial tweets
        button.click();
    }

    /**
     * @param {string} encodedItems
     */
    refreshDisplay(encodedItems)
    {
        this.duplicateCache = [];

        let tweets = [];
        if (encodedItems.length) {
            tweets = JSON.parse(encodedItems);
        }

        this.displayContainer.innerHTML = '';
        let tweetCount = 0;
        tweets.forEach(tweet => {
            if (this.renderTweet(tweet)) {
                tweetCount++;
            }
        });

        if (tweetCount) {
            this.latestId = tweets[0].id;
            this.notification.show(`Showing the latest ${tweetCount} tweet(s)`, 'success');
        } else {
            this.notification.show('No tweets found', 'warning');
        }

        this.interval = setTimeout(() => this.getUpdates(), this.updateInterval);
    }

    /**
     * @param {object} data
     * @param {boolean} before
     */
    renderTweet(data, before = false)
    {
        if (this.duplicateCache.includes(data.id)) {
            return false;
        } else {
            this.duplicateCache.push(data.id);
        }

        let tweet = this.createElementWithContent('section', null, ['tweet']);

        let content = data.text
            .replace(/http/g, ' http')
            .replace(/(https?:\/\/[^ ]+) ?/g, '<a href="$1" target="_blank" class="link">$1</a>')
            .replace(
                /(#([a-z]+))/gi,
                '<a href="https://twitter.com/hashtag/$2?src=hashtag_click" target="_blank" class="hashtag">$1</a>'
            )
            .replace(
                /(@([a-z_0-9]+))/gi,
                '<a href="https://twitter.com/$2" target="_blank" class="user">$1</a>'
            );
        content = this.createElementWithContent('section', content, ['tweet-content']);

        let createdAt = this.createElementWithContent(
            'span',
            new Date(Date.parse(data.created_at)).toLocaleDateString(
                'en',
                {
                    day: '2-digit',
                    month: '2-digit',
                    year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                }
            ),
            ['tweet-created-at']
        );

        tweet.appendChild(content);
        tweet.appendChild(createdAt);

        if (before) {
            this.displayContainer.insertBefore(tweet, this.displayContainer.childNodes[0]);
        } else {
            this.displayContainer.appendChild(tweet);
        }

        return true;
    }

    /**
     * @param {string} tag
     * @param {string} content
     * @param {array} classes
     * @returns {Node}
     */
    createElementWithContent(tag, content, classes = [])
    {
        let node = document.createElement(tag);
        node.innerHTML = content;
        node.classList.add.apply(node.classList, classes);

        return node;
    }

    /**
     * @returns {string|null}
     */
    getUsername()
    {
        return window.localStorage.getItem('username');
    }

    /**
     * @param {string} username
     */
    setUsername(username)
    {
        window.localStorage.setItem('username', username);
    }

    getUpdates()
    {
        const username = this.usernameInput.value;
        this.api.get(username, this.latestId)
            .then(response => this.appendToDisplay(response))
            .catch(response => {
                const data = JSON.parse(response);
                this.notification.show(data.error, 'failure');
                this.interval = setTimeout(() => this.getUpdates(), this.updateInterval);
            });
    }

    /**
     * @param {string} encodedItems
     */
    appendToDisplay(encodedItems)
    {
        let tweets = [];
        if (encodedItems.length) {
            tweets = JSON.parse(encodedItems);
        }

        if (tweets.length) {
            this.latestId = tweets[0].id;

            let tweetCount = 0;
            tweets.reverse().forEach(tweet => {
                if (this.renderTweet(tweet, true)) {
                    tweetCount++;
                }
            });

            if (tweetCount) {
                this.notification.show(`${tweetCount} tweet(s) added`, 'success');
            }
        }

        this.interval = setTimeout(() => this.getUpdates(), this.updateInterval);
    }
}