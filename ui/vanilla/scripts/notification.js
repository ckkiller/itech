class Notification
{
    constructor(container)
    {
        this.container = container;
    }

    show(message, type)
    {
        let notice = document.createElement('section');
        notice.innerHTML = message;
        notice.classList.add('notice', type);

        this.container.appendChild(notice);
        setTimeout(() => this.container.removeChild(notice), 8000);
    }
}