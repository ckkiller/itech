window.onload = () => {
    const tweetDisplay = document.querySelector('.tweets-container');
    const usernameInput = document.querySelector('.user-strip > input');
    const button = document.querySelector('.switch-user-button');
    const loadingIndicator = document.querySelector('.loading');
    const notificationsContainer = document.querySelector('.notifications-container');

    const api = new Api(active => loadingIndicator.classList.toggle('visible', active));
    const notification = new Notification(notificationsContainer);
    new Twitter(api, notification,{ usernameInput, button }, tweetDisplay);
};