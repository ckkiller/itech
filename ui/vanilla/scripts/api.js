class Api
{
    /**
     * @param {function} loading
     */
    constructor(loading)
    {
        this.loading = loading;
    }

    /**
     * @param {string} username
     * @param {string} id
     * @returns {Promise<string>}
     */
    get(username, id)
    {
        this.loading(true);
        let url = `https://api.itech.vanilla/twitter/${username}`;
        if (typeof id !== 'undefined') {
            url += `/${id}`;

        }
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.addEventListener('readystatechange', () => {
                if (xhr.readyState === 4) {
                    if ([200, 204].includes(xhr.status)) {
                        resolve(xhr.responseText);
                    } else {
                        reject(xhr.responseText);
                    }

                    this.loading(false);
                }
            }, false);

            xhr.open('GET', url, true);
            xhr.send();
        });
    }
}