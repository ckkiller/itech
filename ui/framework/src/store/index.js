import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import store from '.';

Vue.use(Vuex);

const domain = 'https://api.itech.framework';

export default new Vuex.Store({
    state: {
        loading: false,
        username: 'ubuntu',
        tweets: [],
        idCache: [],
        latestId: null,
        timeout: null,
        alert: {
            text: '',
            display: false,
            type: 'success',
            timeout: null
        }
    },
    mutations: {
        /**
         * @param {object} state
         * @param {array} payload
         */
        resetTweets(state, payload) {
            state.loading = false;

            if (payload.length) {
                state.tweets = payload;
                state.idCache = payload.map(item => item.id);
                state.latestId = payload[0].id;

                store.commit(
                    'alert',
                    {
                        text: `Showing the latest ${payload.length} tweet(s)`,
                        type: 'success'
                    }
                );
            } else {
                state.tweets = [];
                state.idCache = [];
                state.latestId = null;

                store.commit(
                    'alert',
                    {
                        text: 'No tweets found',
                        type: 'warning'
                    }
                );
            }

            store.commit('setPoll');
        },
        /**
         * @param {object} state
         * @param {array} payload
         */
        prependTweets(state, payload) {
            state.loading = false;

            if (payload.length) {
                payload = payload.filter(item => !state.idCache.includes(item.id));

                if (payload.length) {
                    state.tweets = payload.reverse().concat(state.tweets);
                    state.idCache = state.idCache.concat(payload.map(item => item.id));
                    state.latestId = payload[payload.length - 1].id;

                    store.commit(
                        'alert',
                        {
                            text: `${payload.length} tweet(s) added`,
                            type: 'success'
                        }
                    );
                }
            }

            store.commit('setPoll');
        },
        /**
         * @param {object} state
         */
        startLoading(state) {
            state.loading = true;
        },
        /**
         * @param {object} state
         */
        clearPoll(state) {
            clearTimeout(state.timeout);
        },
        /**
         * @param {object} state
         */
        setPoll(state) {
            state.timeout = setTimeout(() => {
                store.dispatch('update');
            }, 15000);
        },
        /**
         * @param {object} state
         * @param {string} text
         * @param {string} type
         */
        alert(state, { text, type }) {
            clearTimeout(state.alert.timeout);

            state.alert.text = text;
            state.alert.type = type;
            state.alert.display = true;
            state.alert.timeout = setTimeout(() => {
                state.alert.display = false;
            }, 8000);
        },
        /**
         * @param {object} state
         * @param {string} username
         */
        setUsername(state, username) {
            state.username = username;
        }
    },
    actions: {
        /**
         * @param {object} commit
         * @param {object} state
         */
        fetch({ commit, state }) {
            commit('startLoading');
            commit('clearPoll');

            axios.get(`${domain}/twitter/${state.username}`)
                .then(response => commit('resetTweets', response.data))
                .catch(response => commit('alert', { text: response.data.error, type: 'error' }));
        },
        /**
         * @param {object} commit
         * @param {object} state
         */
        update({ commit, state }) {
            commit('startLoading');

            let url = `${domain}/twitter/${state.username}`;
            if (state.latestId !== null) {
                url += `/${state.latestId}`;
            }

            axios.get(url)
                .then(response => commit('prependTweets', response.data))
                .catch(response => commit('alert', { text: response.data.error, type: 'error' }));
        }
    }
});