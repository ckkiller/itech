CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root123';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';

CREATE TABLE vanilla_twitter_auth (
    id int primary key auto_increment,
    token varchar(128),
    requested_at datetime default NOW()
);

CREATE TABLE framework_twitter_auth (
    id int primary key auto_increment,
    token varchar(128),
    requested_at datetime default NOW()
);