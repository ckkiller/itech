service nginx start
service php7.3-fpm start
cd /var/www/api/vanilla && composer install --no-scripts
cd /var/www/api/framework && composer install --no-scripts && bin/console cache:clear
echo "########## Nginx startup complete ##########"
tail -f /var/log/nginx/vanilla_access.log & \
tail -f /var/log/nginx/vanilla_error.log & \
tail -f /var/log/nginx/framework_access.log & \
tail -f /var/log/nginx/framework_error.log
